var tarotApp = angular.module('tarotApp', [ 'ngRoute', 'tarotControllers',
		'ui.bootstrap' ]);

tarotApp.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'partials/start.html',
		controller : 'startController'
	}).when('/form', {
		templateUrl : 'partials/form.html',
		controller : 'formController'
	}).when('/validation', {
		templateUrl : 'partials/validation.html',
		controller : 'validationController'
	}).when('/scores', {
		templateUrl : 'partials/scores.html',
		controller : 'scoresController'
	}).otherwise({
		redirectTo : '/'
	});
} ]);

tarotApp.service('sharedProperties', function() {

	var hashtable = {};

	var scores = {
		gamerNames : [],
		scores : [],
		total : {}
	};

	return {

		reset : function() {
			scores = {
				gamerNames : [],
				scores : [],
				total : {}
			};
		},

		tarot : {
			bids : [ {
				multiplier : 1,
				label : 'Petite'
			}, {
				multiplier : 2,
				label : 'Garde'
			}, {
				multiplier : 4,
				label : 'Garde sans'
			}, {
				multiplier : 6,
				label : 'Garde contre'
			} ],
			
			oudlers : [ {
				treshold : 56,
				label : "Aucun bout"
			}, {
				treshold : 51,
				label : "1 bout"
			}, {
				treshold : 41,
				label : "2 bouts"
			}, {
				treshold : 36,
				label : "3 bouts"
			} ],

			handfuls : [ {
				values : {
					'3' : 13,
					'4' : 10,
					'5' : 8
				},

				label : 'Simple poignée',
				points : 20
			}, {
				values : {
					'3' : 15,
					'4' : 13,
					'5' : 10
				},
				label : 'Double poignée',
				points : 30
			}, {
				values : {
					'3' : 18,
					'4' : 15,
					'5' : 13
				},
				label : 'Triple poignée',
				points : 40
			} ]
		},

		setValue : function(key, value) {
			hashtable[key] = value;
		},
		getValue : function(key) {
			return hashtable[key];
		},
		remove : function(key) {
			setValue(key, null);
		},

		getScores : function() {
			return scores;
		},

		addScore : function(score) {
			scores.scores.push(score);

			for ( var i in score.scores) {
				var gamer = score.scores[i];
				scores.total[gamer.name] += gamer.total;
			}
		}
	}
});