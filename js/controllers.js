var tarotControllers = angular.module('tarotControllers', []);

tarotControllers.controller('startController', function($scope, $location,
		sharedProperties) {
	$scope.gamerNames = {
		count : 0,
		names : [ {
			name : ''
		}, {
			name : ''
		}, {
			name : ''
		}, {
			name : ''
		}, {
			name : ''
		} ]
	};
	sharedProperties.reset();

	var findDuplicates = function(array) {
		for (var i = 0; i < array.length; i++)
			for (var j = i + 1; j < array.length; j++) {
				if (array[i] == array[j])
					return true;
			}
		return false;
	}

	$scope.submit = function() {
		var g = $scope.gamerNames;
		sharedProperties.reset();
		var scores = sharedProperties.getScores();
		for (var i = 0; i < g.count; i++) {
			var name = g.names[i].name;
			scores.gamerNames.push(name);
			scores.total[name] = 0;
		}
		var result = findDuplicates(scores.gamerNames);
		if (result) {
			$scope.error = "Duplicates names";
		} else {
			$location.path("/scores");
		}
	}
});

tarotControllers.controller('formController',
		function($scope, sharedProperties) {

			$scope.form = {
				gamerNames : sharedProperties.getScores().gamerNames,
				taker : '',
				partner : '',
				bid : '',
				treshold : 0,
				points : '',
				smallAtEnd : '',
				handful : 0,
				misery : ''
			};
			sharedProperties.setValue('form', $scope.form);
			$scope.tarot = sharedProperties.tarot;
		});

tarotControllers.controller('scoresController', function($scope,
		sharedProperties) {
	// $scope.gamers = sharedProperties.getValue('gamers');
	$scope.scores = sharedProperties.getScores();

});

tarotControllers.controller('validationController', function($scope,
		sharedProperties) {
	var form = sharedProperties.getValue('form');
	var tarot = sharedProperties.tarot;
	var gamerNames = sharedProperties.getScores().gamerNames;

	var findElement = function(array, field, value) {
		for ( var i in array) {
			var e = array[i];
			if (array[i][field] == value)
				return array[i];
		}
	};

	var bid = findElement(tarot.bids, 'label', form.bid);
	var treshold = form.treshold;

	var diff = form.points - form.treshold;
	var success = diff >= 0;
	var points = success ? diff + 25 : diff - 25;
	var hasPartner = form.partner && form.partner != '';
	var playerCount = gamerNames.length;

	var stage = {
		taker : form.taker,
		partner : form.partner,
		bid : bid,
		treshold : treshold,
		donePoints : form.points,
		points : diff,
		scores : []
	};
	$scope.stage = stage;

	var compute = function(gamerName) {
		var isTaker = gamerName == form.taker;
		var isPartner = gamerName == form.partner;

		var multiplier = -1;
		if (!hasPartner) {
			if (isTaker)
				multiplier = playerCount - 1;
		} else {
			if (isTaker)
				multiplier = 2;
			else if (isPartner)
				multiplier = 1;
		}

		var small = 0;
		hasSmall = (form.smallAtEnd && form.smallAtEnd != '');
		if (hasSmall) {
			var m = 'A' == form.smallAtEnd ? 1 : -1;
			small = 10 * (multiplier * m);
		}

		multiplier *= success ? 1 : -1;

		var handful = form.handful * multiplier;

		var misery = 0;
		var hasMisery = form.misery && form.misery != '';
		if (hasMisery)
			misery = gamerName == form.misery ? 10 * (playerCount - 1) : -10;
		var pts = Math.abs(points) * multiplier;
		var total = (pts + small) * bid.multiplier + handful + misery;

		return {
			points : pts,
			multiplier : multiplier,
			small : small,
			handful : handful,
			misery : misery,
			name : gamerName,
			isTaker : isTaker,
			isPartner : isPartner,
			total : total
		}
	};

	for ( var i in gamerNames) {
		var name = gamerNames[i];
		stage.scores.push(compute(name));
	}
	$scope.addScore = function() {
		sharedProperties.addScore($scope.stage);
	}

});